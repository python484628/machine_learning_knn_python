# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# January 2021


"""
**KNN with Python**
"""

from sklearn.datasets import load_breast_cancer
cancer = load_breast_cancer()

"""Show a summary of the dataset using pandas

"""

import numpy as np
import pandas as pd 
data = pd.DataFrame(cancer.data, columns = [cancer.feature_names])
data ['Target'] = pd.Series (data=cancer.target, index = data.index)
print("target indexes and target names are %s" %(list(zip(list(set(cancer.target)), cancer.target_names))))
data.head()

data.info()

data.describe()

"""**K-nearest neighbors**

Create the classifier
"""

from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier()

"""Perform a 5CV on the breast cancer dataset"""

from sklearn.model_selection import cross_val_score
from sklearn import metrics
# Perform a 5 fold cross validation
scores = cross_val_score(knn, cancer.data, cancer.target, cv = 5)
print ("Cross-validated scores :", scores)
print ("Average 5CV score is %f +- %f" %(scores.mean(), scores.std()))

"""**Effect of the number of nearest neighbors**

We'll create a train test split
"""

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(cancer.data, cancer.target)

"""Then, we'll get the accuracy of KNN with higher numbers of neighbors and plot the results

Question : 
Try the same code with higher values for max_k
"""

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline 

## K will vary from 1 to a max_k value
max_k=20
max_k+=1
accuracy = []
for n in range(1,max_k):
  knn = KNeighborsClassifier(n_neighbors = n)
  knn.fit(X_train, y_train)
  y_pred = knn.predict(X_test)
  accuracy.append(accuracy_score(y_test, y_pred))
# Plotting the accuracy for different values of k
print(accuracy)
plt.figure(figsize=(10,5))
plt.plot(range(1,max_k), accuracy, label = "KNN Accuracy", marker = 'o', markerfacecolor ='blue', color = 'skyblue', linewidth =4)
plt.legend()

"""**Effect of the weight on the distance**"""

# Commented out IPython magic to ensure Python compatibility.
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
# %matplotlib inline

## K will vary from 1 to a max_k value
max_k= 50
max_k+=1
accuracy, accuracy2 = [], []
for n in range(1,max_k) :
  knn = KNeighborsClassifier(n_neighbors = n, weights ="uniform")
  knn.fit(X_train, y_train)
  y_pred = knn.predict(X_test)
  accuracy.append(accuracy_score(y_test, y_pred))

  knn = KNeighborsClassifier(n_neighbors = n, weights ="distance")
  knn.fit(X_train, y_train)
  y_pred = knn.predict(X_test)
  accuracy2.append(accuracy_score(y_test, y_pred))
 ## Plotting the accuracies for different values of k
print(accuracy)
plt.figure(figsize=(10,5))
plt.plot(accuracy, marker = 'o', markerfacecolor = 'blue', color = 'skyblue', linewidth =4, label = "weights:uniform")
plt.plot(accuracy2, marker = 'X', color = 'red', linewidth=2, label = "weights:distance")
plt.legend()

"""**Effect of the distance measure**"""

from sklearn.metrics import accuracy_score

d_measures = ["euclidean", "manhattan", "chebyshev", "minkowski"]
accuracy= []
for d in d_measures : 
  knn = KNeighborsClassifier(metric=d)
  knn.fit(X_train, y_train)
  y_pred = knn.predict(X_test)
  accuracy.append(accuracy_score(y_test, y_pred))
## Plotting the accuracies for the different distance measures
print(accuracy)
plt.bar(d_measures, accuracy, width = 0.35)
plt.ylim([0.85, 1])
plt.ylabel('Accuracy')

"""**Effect of feature normalization**

Let's try measuring the accuracy again with feature normalization and compare
"""

from sklearn import preprocessing
from sklearn.model_selection import cross_val_score
from sklearn import metrics

knn = KNeighborsClassifier()

# Normalize the data attributes
scaled_X = preprocessing.minmax_scale(cancer.data)

# Perform 5-fold cross validation
scores = cross_val_score(knn, cancer.data, cancer.target, cv=5)
print ("Cross-validated scores:", scores)
print ("Average 5CV score is %f +-%f(std)" %(scores.mean(), scores.std()))

# Perform 5-fold cross validation
scores = cross_val_score(knn, scaled_X, cancer.target, cv=5)
print ("Cross-validated scores (scaled):", scores)
print ("Average 5CV score (scaled) is %f +-%f(std)" %(scores.mean(), scores.std()))
