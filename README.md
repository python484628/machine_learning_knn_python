***KNN with python***

**Step 1**

Create the classifier.

**Step 2**

Perform a 5CV on the breast cancer dataset.

**Step 3**

Effect of the number of nearest neighbors.

**Step 4**

Effect of the weight on the distance.

**Step 5**

Effect of the distance measure.

**Step 6**

Effect of feature normalization.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

January 2021
